var cors=require('cors');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
//xinjiade
const user = require("./routes/user");
const blog = require("./routes/blog");
const comment = require("./routes/comment");


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use(cors());

//xinjiade
app.get('/user/:id/findById',user.findById);     //0
app.get('/user/:name/findByName',user.findByName);
app.post('/user/registration',user.addUser);   //1
app.post('/user/login',user.checkUser);    //0
app.delete('/user/:id/delete',user.deleteUser);   //1
app.put('/user/:id/alterPassword',user.alterPassword); //0
app.get('/user/findAllUser',user.findAllUser); //0


app.post('/blog/:userId/add',blog.addBlog);     //1
app.delete('/blog/:id/delete',blog.deleteBlog);  //1
app.put('/blog/:id/alter',blog.alterBlog);   //0
app.get('/blog/:userId/findByUserId',blog.findBlogByUser);//0
app.get('/blog/:keyword/searchBlog',blog.searchBlog); //0
app.get('/blog/findAll',blog.findAll); //0


app.post('/comment/:userId/add/:blogId',comment.addComment);
app.get('/comment/:blogId/findByBlogId',comment.findCommentByBlog);
app.get('/comment/:userId/findByUserId',comment.findCommentByUser);
app.get('/comment/:id/findById',comment.findComment);
app.put('/comment/:id/alter',comment.alterComment);
app.delete('/comment/:id/delete',comment.deleteComment);






// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;


