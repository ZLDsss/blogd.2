Client GitLab repo: https://gitlab.com/ZLDsss/blogvuef




Web API Gitlab repo: https://gitlab.com/ZLDsss/blogd.2
Web API Production URL: https://blogsyf-prod-api.herokuapp.com/
Web API Staging URL: https://blogsyf-staging-api.herokuapp.com/






[![pipeline status](https://gitlab.com/ZLDsss/blogd.2/badges/master/pipeline.svg)](https://gitlab.com/ZLDsss/blogd.2/commits/master)

[![coverage report](https://gitlab.com/ZLDsss/blogd.2/badges/master/coverage.svg)](https://gitlab.com/ZLDsss/blogd.2/badges/master/coverage.svg?job=coverage)

# Assignment 2 - Agile Software Practice.

Name: YIFAN SUN    (Englishname:Archie)

## Client UI.
![Image text](img/register.png)
>>Allows the user to register

![Image text](img/login.png)
>>Allows the user to login

![Image text](img/addBlog.png)
>>Allows the user to add blogs

![Image text](img/showAllUsers.png)
>>show the result of all users

![Image text](img/showAllblogs.png)
>>show the result of all blogs


![Image text](img/resultOfSeachingOtherUser.png)
>>show the result of searching other user

![Image text](img/makeComment.png)
>>Allows the user to make commment


## E2E/Cypress testing.


## Web API CI.
https://zldsss.gitlab.io/blogd.2/
![Image text](img/coverageReport.png)


## GitLab CI.

Published test execution(Cypress dashboard):https://dashboard.cypress.io/projects/tkgxfd/runs/4/specs


