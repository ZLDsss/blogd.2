let express=require('express');
let router=express.Router();
let mongoose =require('mongoose');
const User=require("../models/users");
// cons
// const Blog=require("../models/blogs");
let mongodbUri="mongodb+srv://Archie:sunyifanqwert@cluster0-mxku3.mongodb.net/blogdb?retryWrites=true&w=majority";
mongoose.connect(mongodbUri);
let db = mongoose.connection;

db.on('error', function (err) {
    console.log('Unable to Connect to [ ' + db.name + ' ]', err);
});

db.once('open', function () {
    console.log('Successfully Connected to [ ' + db.name + ' ]');
});

//method
//register
router.addUser = (req,res) => {
    // res.setHeader('Content-Type', 'application/json');
    let user=new User();
    user.account=req.body.account;
    user.password=req.body.password;
    user.name=req.body.name;
    user.save(function (err) {
        if(err)
            res.json({ message: "Add unsuccessfully" });
        else{
            res.json({ message: "Add successfully" });
        }
    })
}
//login
router.checkUser=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    let account=req.body.account;
    let password=req.body.password;
    User.find({"account":account},function(err, ur) {
        if (err)
            res.send(err);
        else{
            if(ur[0].password==password){
                res.json({ message: "Login successfully",name:ur[0].name });
            }else{
                res.json({ message: "Wrong password" });
            }
        }
    });
}
//findById
router.findById=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    User.find({"_id":req.params.id},function (err,ur) {
        if(err){
            res.json({message:"Not Exist"});
        }else{
            res.send(JSON.stringify(ur[0],null,5));
        }
    })
}
//finall
router.findAllUser = (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    User.find(function(err, bls) {
        if (err) {res.send(err);}
        res.send(JSON.stringify(bls,null,5));
    });
};
//findByName
router.findByName=(req,res)=>{
    res.setHeader('Content-Type', 'application/json');
    User.find({"name":req.params.name},function (err,ur) {
        if(err){
            res.json({message:"Not Exist"});
        }else{
            res.send(JSON.stringify(ur[0],null,5));
        }
    })
}
//deleteUser
router.deleteUser=(req,res)=>{
    User.findByIdAndDelete(req.params.id,function(err,ur){
        if(err){
            res.json({ message: "Delete unsuccessfully" });
        }else {
            res.json({ message: "Delete successfully" });
        }
    })
}
//AlterPassword
router.alterPassword=(req,res)=>{
    let newPassword=req.body.password;
    User.findById(req.params.id,function(err,ur){
        if(err){
            res.json({ message: "Not Exist" });
        }else {
            ur.password=newPassword;
            ur.save(function(err){
                if(err){
                    res.json({ message: "Not Exist" });
                }else{
                    res.json({message:"Alter successfully"});
                }
            })
        }
    })
}




module.exports = router;