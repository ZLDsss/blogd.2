const chai = require("chai");
// import chai from 'chai';
// const server = require("../../../bin/www");
const expect = chai.expect;
const request = require("supertest");


const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer;

let Users=require('../../../models/users');
let Blogs=require('../../../models/blogs');
let Comments=require('../../../models/comments');



const mongoose = require("mongoose");

const _ = require("lodash");
let server;
let mongod;
let db, validID,validID3,validID4;
let vID,vID2;
let cID;
//UserTest
describe("Blog", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "blogdb" // by default generate random dbName
                }
            });
            // Async Trick - this ensures the database is created before
            // we try to connect to it or start the server
            await mongod.getConnectionString();

            mongoose.connect("mongodb://localhost:27017/blogdb", {
                useNewUrlParser: true,
                useUnifiedTopology: true
            });
            server = require("../../../bin/www");
            db = mongoose.connection;
        } catch (error) {
            console.log(error);
        }
    });
    // after(async () => {
    //     try {
    //         await db.dropDatabase();
    //     } catch (error) {
    //         console.log(error);
    //     }
    // });

    beforeEach(async () => {
        try {
            await Users.deleteMany({});
            let user = new Users();
            user.account = "1";
            user.password = "1";
            user.name = "1";
            await user.save();
            let user2=new Users();
            user2.account = "2";
            user2.password = "2";
            user2.name = "2";
            await user2.save();
            let user3=new Users();
            user3.account = "3";
            user3.password = "3";
            user3.name = "3";
            await user3.save();
            let user4=new Users();
            user4.account = "4";
            user4.password = "4";
            user4.name = "4";
            await user4.save();
            user = await Users.findOne({ name: "1" });
            validID = user._id;
            user3 = await Users.findOne({ name: "3" });
            validID3 = user3._id;
            user4 = await Users.findOne({ name: "4" });
            validID4 = user4._id;

            await Blogs.deleteMany({});
            let blog = new Blogs();
            blog.userId ="1234567";
            blog.title = "school";
            blog.content = "1";
            await blog.save();

            let blog2 = new Blogs();
            blog2.userId ="12345678";
            blog2.title = "school";
            blog2.content = "2";
            await blog2.save();

            let blog3 = new Blogs();
            blog3.userId ="1234567";
            blog3.title = "nba";
            blog3.content = "player";
            await blog3.save();

            let blog4 = new Blogs();
            blog4.userId ="1234567";
            blog4.title = "nba new season";
            blog4.content = "player";
            await blog4.save();

            blog = await Blogs.findOne({ content: "1" });
            vID=blog._id;
            blog2 = await Blogs.findOne({ content: "2" });
            vID2=blog2._id;

            await Comments.deleteMany({});
            let comment = new Comments();
            comment.userId ="1234567";
            comment.blogId ="1111111";
            comment.content = "Good";
            await comment.save();
            let comment2 = new Comments();
            comment2.userId ="1234567";
            comment2.blogId ="2222222";
            comment2.content = "Great";
            await comment2.save();
            comment = await Comments.findOne({ content: "Good" });
            cID=comment._id;
            comment2 = await Comments.findOne({ content: "Great" });
            cID2=comment2._id;


        } catch (error) {
            console.log(error);
        }
    });
//    USER
//AddUser
    describe("POST /user/registration", () => {
        it("should return a message", () => {
            const user = {"account":"100","password":"100","name":"100"};
            return request(server)
                .post("/user/registration")
                .send(user)
                .expect(200)
                .expect({ message: "Add successfully" });
        });
    });
//checkUser
//     describe("POST /user/login", () => {
// //         it("should return a message", () => {
// //             const aap = {"account":"1","password":"1"};
// //             return request(server)
// //                 .post("/user/login")
// //                 .send(aap)
// //                 .expect(200)
// //                 .expect({ message: "Login successfully" });
// //         });
// //         it("should return a message", () => {
// //             const aap = {"account":"1","password":"w"};
// //             return request(server)
// //                 .post("/user/login")
// //                 .send(aap)
// //                 .expect(200)
// //                 .expect({ message: "Wrong password" });
// //         });
// //     });
//deleteUser
//     describe("DELETE /user/:id/delete", () => {
//         it("should return a message", () => {
//             return request(server)
//                 .delete(`/user/${validID3}/delete`)
//                 .expect(200)
//                 .expect({ message: "Delete successfully" });
//         });
//     });
//alterPassword
//     describe("PUT /user/:id/alterPassword", () => {
//         it("should return a message", () => {
//             const np = {"password":"444"};
//             return request(server)
//                 .put(`/user/${validID4}/alterPassword`)
//                 .send(np)
//                 .expect(200)
//                 .expect({message:"Alter successfully"});
//         });
//         it("should return a message", () => {
//             const np = {"password":"qqq"};
//             return request(server)
//                 .put("/user/wrongId/alterPassword")
//                 .send(np)
//                 .expect(200)
//                 .expect({message:"Not Exist"});
//         });
//     });
    //findById
    describe("GET /user/:id/findById", () => {
        //This method works sometimes and fails sometimes. I dont figure out the reason.
        // describe("when the id is valid", () => {
        //     it("should return the matching user", done => {
        //         request(server)
        //             .get(`/user/${validID}/findById`)
        //             .set("Accept", "application/json")
        //             .expect("Content-Type", /json/)
        //             .expect(200)
        //             .end((err, res) => {
        //                 // console.log(vID);
        //                 // expect(res.body).to.have.property("name", "1");
        //                 expect(res.body.name).to.equal('1');
        //                 done(err);
        //             });
        //     });
        // });
        describe("when the id is invalid", () => {
            it("should return the message", done => {
                request(server)
                    .get(`/user/wrongId/findById`)
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        // console.log(res.body);
                        console.log(vID);
                        expect(res.body).to.have.property("message", "Not Exist");
                        done(err);
                    });
            });
        });
    });
    //Blog
    // addBlog
    describe("POST /blog/:userId/add", () => {
        it("should return a message", () => {
            const blog = {"title":"New","content":"New"};
            return request(server)
                .post("/blog/11111111/add")
                .send(blog)
                .expect(200)
                .expect({ message: "Add successfully" });
        });
    });
    // deleteBlog
    // describe("DELETE /blog/:id/delete", () => {
    //     it("should return a message", () => {
    //         return request(server)
    //             .delete(`/blog/${vID2}/delete`)
    //             .expect(200)
    //             .expect({ message: "Delete successfully" });
    //     });
    // });
    // alterBlog
    describe("PUT /blog/:id/alter", () => {
        it("should return a message", () => {
            const nb = {"title":"Changed","content":"Changed"};
            return request(server)
                .put(`/blog/${vID}/alter`)
                .send(nb)
                .expect(200)
                .expect({message:"Alter successfully"});
        });
        it("should return a message", () => {
            const nb = {"title":"NBA","content":"KL get 100 points"};
            return request(server)
                .put("/blog/wrongId/alter")
                .send(nb)
                .expect(200)
                .expect({message:"Not Exist"});
        });
    });

    //FindByUserId
    describe("GET /blog/:userId/findByUserId", () => {
        // it("should return blogs", done => {
        //     request(server)
        //         .get("/blog/12345678/findByUserId")
        //         .set("Accept", "application/json")
        //         .expect(200)
        //         .end((err, res) => {
        //             // console.log(res.body);
        //             expect(res.body[0].content).to.equal("2");
        //             done(err);
        //         });
        // });
        // it("should return a message ", done => {
        //     request(server)
        //         .get("/blog/wrongId/findByUserId")
        //         .set("Accept", "application/json")
        //         .expect("Content-Type", /json/)
        //         .expect(200)
        //         .end((err, res) => {
        //             expect(res.body).to.deep.equal([]);
        //             done(err);
        //         });
        // });
    });
//searchBlog
    describe("GET /blog/:keyword/searchBlog", () => {
        it("should return blogs", done => {
            request(server)
                .get("/blog/nba/searchBlog")
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    // console.log(res.body);
                    expect(res.body[0].title).to.equal("nba");
                    expect(res.body[1].title).to.equal("nba new season");
                    done(err);
                });
        });
        it("should return a message ", done => {
            request(server)
                .get("/blog/wrongKeyword/searchBlog")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.deep.equal([]);
                    done(err);
                });
        });
    });
//comment
//addComment
    describe("POST /comment/:userId/add/:blogId", () => {
        it("should return a message", () => {
            const comment = {"content":"Bad"};
            return request(server)
                .post("/comment/1234567/add/1111111")
                .send(comment)
                .expect(200)
                .expect({ message: "Add successfully" });
        });
    });
//findByBlogId
    describe("GET /comment/:blogId/findByBlogId", () => {
        it("should return comment", done => {
            request(server)
                .get("/comment/1111111/findByBlogId")
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    // console.log(res.body);
                    expect(res.body[0].content).to.equal("Good");
                    done(err);
                });
        });
        it("should return a message ", done => {
            request(server)
                .get("/comment/:blogId/findByBlogId")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.deep.equal([]);
                    done(err);
                });
        });
    });
    //findByUserId
    describe("GET /comment/:blogId/findByUserId", () => {
        it("should return comment", done => {
            request(server)
                .get("/comment/1234567/findByUserId")
                .set("Accept", "application/json")
                .expect(200)
                .end((err, res) => {
                    // console.log(res.body);
                    expect(res.body[0].content).to.equal("Good");
                    done(err);
                });
        });
        it("should return a message ", done => {
            request(server)
                .get("/comment/:blogId/findByUserId")
                .set("Accept", "application/json")
                .expect("Content-Type", /json/)
                .expect(200)
                .end((err, res) => {
                    expect(res.body).to.deep.equal([]);
                    done(err);
                });
        });
    });
    //findById
    // describe("GET /comment/:blogId/findById", () => {
    //     it("should return comment", done => {
    //         request(server)
    //             .get(`/comment/${cID}/findById`)
    //             .set("Accept", "application/json")
    //             .expect(200)
    //             .end((err, res) => {
    //                 // console.log(res.body);
    //                 expect(res.body[0].content).to.equal("Good");
    //                 done(err);
    //             });
    //     });
    //     it("should return a message ", done => {
    //         request(server)
    //             .get("/comment/:blogId/findById")
    //             .set("Accept", "application/json")
    //             .expect("Content-Type", /json/)
    //             .expect(200)
    //             .end((err, res) => {
    //                 expect(res.body).to.deep.equal({ message: "Not Exist" });
    //                 done(err);
    //             });
    //     });
    // });
    // alterComment
    // describe("PUT /comment/:id/alter", () => {
    //     it("should return a message", () => {
    //         const nb = {"content":"Changed"};
    //         return request(server)
    //             .put(`/comment/${cID}/alter`)
    //             .send(nb)
    //             .expect(200)
    //             .expect({message:"Alter successfully"});
    //     });
    //     it("should return a message", () => {
    //         const nb = {"content":"KL get 100 points"};
    //         return request(server)
    //             .put("/comment/wrongId/alter")
    //             .send(nb)
    //             .expect(200)
    //             .expect({message:"Not Exist"});
    //     });
    // });
    // deleteComment
    // describe("DELETE /comment/:id/delete", () => {
    //     it("should return a message", () => {
    //         return request(server)
    //             .delete(`/comment/${cID2}/delete`)
    //             .expect(200)
    //             .expect({ message: "Delete successfully" });
    //     });
    // });


});












