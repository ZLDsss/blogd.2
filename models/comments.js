let mongoose = require('mongoose');

let CommentSchema = new mongoose.Schema({
        userId: String,
        blogId: String,
        content: String,
        time: String,
    },
    { collection: 'comments' });

module.exports = mongoose.model('Comment', CommentSchema);